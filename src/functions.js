const CryptoJS = require("crypto-js");

const shiftCarDown = (n, car) => {
  const max = 125;
  const min = 32;
  const delta = max - min + 1;

  const asciiValue = car.charCodeAt();
  const newAsciiValue = ((asciiValue - min - n) % delta) + max + 1;
  const result = String.fromCharCode(newAsciiValue);

  return result;
};

export const cubbitDecrypt = (key, text) => {
  const result = [];
  const keySize = key.length;
  const keySum = [...key].reduce((tot, car) => tot + car.charCodeAt(), 0);
  let chunks = [];
  for (let i = 0; i < text.length; i += keySize) {
    const chunk = text.slice(i, i + keySize);
    chunks.push(chunk);
  }

  for (let chunk of chunks) {
    const revChunk = [...chunk].reverse();
    const chunkAfterShift = revChunk.map((car) => shiftCarDown(keySum, car));
    const decryptedChunk = [...chunkAfterShift].reverse();
    result.push(...decryptedChunk);
  }
  return result.join("");
};

export const decode64 = (base64) => {
  const words = CryptoJS.enc.Base64.parse(base64);
  const result = CryptoJS.enc.Utf8.stringify(words);
  return result;
};

export const encode64 = (textString) => {
  const words = CryptoJS.enc.Utf8.parse(textString);
  const result = CryptoJS.enc.Base64.stringify(words);
  console.log("encode64 ", textString, result);
  return result;
};

export function convertWordArrayToUint8Array(wordArray) {
  var arrayOfWords = wordArray.hasOwnProperty("words") ? wordArray.words : [];
  var length = wordArray.hasOwnProperty("sigBytes")
    ? wordArray.sigBytes
    : arrayOfWords.length * 4;
  var uInt8Array = new Uint8Array(length),
    index = 0,
    word,
    i;

  for (i = 0; i < length; i++) {
    word = arrayOfWords[i];
    uInt8Array[index++] = word >> 24;
    uInt8Array[index++] = (word >> 16) & 0xff;
    uInt8Array[index++] = (word >> 8) & 0xff;
    uInt8Array[index++] = word & 0xff;
  }

  return uInt8Array;
}

export const decryptAES = (key, encryptedFile) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = () => {
      const decrypted = CryptoJS.AES.decrypt(reader.result, key); // Decryption: I: Base64 encoded string (OpenSSL-format) -> O: WordArray
      const typedArray = convertWordArrayToUint8Array(decrypted); // Convert: WordArray -> typed array
      const fileDec = new Blob([typedArray]); // Create blob from typed array
      resolve(fileDec); // Create blob from string
    };
    reader.onerror = function (e) {
      reject(e);
    };
    reader.readAsText(encryptedFile);
  });
};

export const encryptAES = async (key, htmlFile) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = () => {
      const wordArray = CryptoJS.lib.WordArray.create(reader.result); // Convert: ArrayBuffer -> WordArray
      const encrypted = CryptoJS.AES.encrypt(wordArray, key).toString(); // Encryption: I: WordArray -> O: -> Base64 encoded string (OpenSSL-format)
      const fileEnc = new Blob([encrypted]);
      resolve(fileEnc); // Create blob from string
    };
    reader.onerror = function (e) {
      reject(e);
    };
    reader.readAsArrayBuffer(htmlFile);
  });
};
