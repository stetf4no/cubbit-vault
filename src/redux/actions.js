import store from './store';
import {
  SET_PLAIN_MODE,
} from './action-types';

export const setPlainModeAction = value => ({
  type: SET_PLAIN_MODE,
  value,
});



