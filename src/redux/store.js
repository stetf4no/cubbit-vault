import { createStore, compose } from 'redux';
import reducers from './reducers';



const store = createStore(reducers);


// You can use subscribe() to update the UI in response to state changes.
// Normally you'd use a view binding library (e.g. React Redux) rather than subscribe() directly.
// However it can also be handy to persist the current state in the localStorage.

store.subscribe(() => console.log("New state is: ",store.getState()));

export default store;
