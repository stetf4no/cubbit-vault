/* eslint-disable import/no-anonymous-default-export */
/* eslint-disable default-case */
import { SET_PLAIN_MODE } from "./action-types";

export default (state = { plainMode: false, encryptKey: 'fullstack' }, action) => {
  console.log('reducer old state ',state);
  let newState = {...state};
  switch (action.type) {
    case SET_PLAIN_MODE:
      newState = { ...state, plainMode: action.value };
      break;
  }
  console.log('reducer new state ',newState);
  return newState;
};
