import "./fileDecrypt.css";
import { useState } from "react";
import axios from "axios";
import { decode64, decryptAES } from "../functions";
import { toast } from "react-toastify";



import { saveAs } from "file-saver";

const serviceUrl = `${process.env.REACT_APP_SERVER_URL}/v1/files`;

const FileDecrypt = (props) => {
  console.log("FileDecrypt ", props);
  const { fileMetaData } = props;
  const { id, name, size, mimetype } = fileMetaData;
  const [base64Key, setBase64Key] = useState();

  const handleChange = function (e) {
    const b64key = e.target.value;
    console.log("b64key ", b64key);
    setBase64Key(b64key);
  };

  const getWholeFile = async () => {
    try {
      const response = await axios.get(`${serviceUrl}/${id}`, {
        responseType: "blob",
      });
      const blob = await response.data;
      const AESKey = decode64(base64Key);
      const fileDec = await decryptAES(AESKey, blob);
      console.log("File decrypted using key ", AESKey);
      saveAs(fileDec, name);  
    }
    catch(ex) {
      console.error(ex);
    }
  };

  return (
    <div>
      <div className="fileData" id="input1">
        <p>{id}</p>
      </div>
      <h2 id="fileid">File id:</h2>
      <div className="fileData" id="input2">
        <p>{name}</p>
      </div>
      <h2 id="fileName">File name:</h2>
      <div className="fileData" id="input3">
        <p>{size}</p>
      </div>
      <h2 id="fileSize">File size:</h2>
      <div className="fileData" id="input4">
        <p>{mimetype}</p>
      </div>
      <h2 id="fileMime">File mime:</h2>
      <h2 id="insert">Insert your encryption key:</h2>
      <input
        type="text"
        className="fileData"
        id="input5"
        onChange={handleChange}
      ></input>
      <button id="button" type="button" onClick={getWholeFile}>
        <p>Decrypt and download</p>
      </button>
    </div>
  );
};

export default FileDecrypt;
