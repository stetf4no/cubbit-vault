import "./fileIdCheck.css";
import axios from "axios";
import { useState } from "react";
import { toast } from "react-toastify";

const serviceUrl = `${process.env.REACT_APP_SERVER_URL}/v1/files`;
const FileIdCheck = (props) => {
  const { done } = props;
  const [fileId, setFileId] = useState();

  const onChange = function (e) {
    setFileId(e.target.value);
  };

  const getFileMetadata = () => {
    // Create an object of formDat
    axios
      .get(`${serviceUrl}/${fileId}/meta`)
      .then((res) => {
        console.log("getFileMetadata ", res.data);
        done({ fileMetaData: { ...res.data, id: fileId }, page: 31 });
      })
      .catch((ex) => {
        console.error(ex);
        toast.error("File not found.");
      });
  };

  return (
    <div>
      <h2 id="fileId">Insert your file id:</h2>
      <input type="text" id="inputText" onChange={onChange} />
      <button id="getFile" type="button" onClick={getFileMetadata}>
        <p>Get file</p>
      </button>
    </div>
  );
};

export default FileIdCheck;
