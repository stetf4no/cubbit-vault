import React, { useState } from "react";
import logo from "../assets/Files.svg";
import fileLogo from "../assets/FilesBig.svg";
import divider from "../assets/Divider.svg";
import arrow from "../assets/ArrowDown.svg";
import "./fileUploader.css";
import EncryptedText from "./EncryptedText";
import axios from "axios";
import { nanoid } from "nanoid";
import { encryptAES } from "../functions";
const sendFileUrl = `${process.env.REACT_APP_SERVER_URL}/v1/files`;

const FileUploader = (props) => {
  const generateKey = () => nanoid();

  const title = `^#5 -"$#=.-+(-$=%(+$=$-"18/3(.-= -#=#$"18/3(.-K=p$"41$= -8=%(+$=38/$= -#=, (-3 (-=8.41=/1(5 "8>`;
  const buttonText = "`'..2$=%(+$>";
  const { encryptKey, plainMode, done } = props;
  const [plainFile, setPlainFile] = useState({});
  const [AESKey, setAESKey] = useState(generateKey());

  const storeSelectedFile = (e) => {
    e.preventDefault();
    const newFileData =
      (e && e.target && e.target.files && e.target.files[0]) || {};
    console.log("newFileData :", newFileData);
    setPlainFile(newFileData);
  };

  const onDrop = (e) => {
    e.preventDefault();
    const items = e.dataTransfer.items;
    const file = items[0].getAsFile();
    setPlainFile(file);
  };

  const uploadFile = async () => {
    console.log("File uploading...", plainFile);
    const encryptedFile = await encryptAES(AESKey, plainFile);

    const formData = new FormData();
    // Update the formData object
    let file = new File([encryptedFile], plainFile.name, {
      type: plainFile.type,
      lastModified: plainFile.lastModified,
    });

    formData.append("cubbitfile", file);
    const config = {
      headers: {
        type: "formData",
        mimetype: plainFile.type,
        filename: plainFile.name,
        Accept: "application/json",
      },
    };
    axios
      .post(sendFileUrl, formData, config)
      .then((res) => {
        console.log("uploadFile ", res);
        done({
          fileData: {
            id: res.data.id,
            name: plainFile.name,
            key: AESKey,
          },
          page: 2,
        });
      })
      .catch((ex) => {
        console.error(ex);
        done({ err: ex });
      });
  };

  return (
    <div>
      <h3 id="h3">
        <EncryptedText
          encryptKey={encryptKey}
          plainMode={plainMode}
          value={title}
        />
      </h3>
      <div id="dropzone_large">
        <div
          id="backGround_drop"
          onDrop={onDrop}
          onDragOver={(e) => e.preventDefault()}
        >
          <input
            accept="*"
            id="file"
            multiple={false}
            type="file"
            name="file"
            onChange={(e) => storeSelectedFile(e)}
            hidden
          />
          <label htmlFor="file" className="input" id="buttonOverDiv"></label>
          <div
            className="input"
            id="divUnderButton"
            hidden={plainFile.name ? true : false}
          >
            <img src={logo} id="file" alt="" />
            <p id="textUpload">
              <EncryptedText
                encryptKey={encryptKey}
                plainMode={plainMode}
                value={buttonText}
              />
            </p>
            <img src={divider} id="solid" alt=""></img>
            <img src={arrow} id="arrow" alt=""></img>
          </div>
          <div id="withFile">
            <img
              src={fileLogo}
              id="fileLogo"
              hidden={plainFile.name ? false : true}
              alt=""
            />
            {plainFile.name && <h2 id="dropText">{plainFile.name}</h2>}
          </div>
        </div>
        {plainFile.name ? null : <h2 id="dropText">or drop a file here</h2>}
      </div>
      <div id="actions">
        <button
          type="button"
          disabled={!plainFile.name}
          onClick={uploadFile}
          id="encrypt"
        >
          <p id="upload">Encrypt and upload</p>
        </button>
        <button type="button" id="decrypt" onClick={() => done({ page: 3 })}>
          <p id="download">Download and decrypt</p>
        </button>
      </div>
    </div>
  );
};

export default FileUploader;
