import React from "react";
import { connect } from "react-redux";
import { cubbitDecrypt } from "../functions";

const EncryptedText = (props) => {
  const { plainMode, encryptKey, value } = props;
  console.log("key value ", encryptKey, value);
  return (
    <span>
      {encryptKey && plainMode ? cubbitDecrypt(encryptKey, value) : value}
    </span>
  );
};


export default EncryptedText;
