import "./fileData.css";
import fileLogo from "../assets/FilesWhite.svg";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { encode64 } from "../functions";

const FileData = (props) => {
  const { fileData } = props;
  const { name, id, key } = fileData;

  return (
    <div>
      <div id="borderBox">
        <div id="logoText">
          <img src={fileLogo} id="fileLogo" alt="" />
          <p id="filename">{name}</p>
        </div>
      </div>
      <h2 id="fileId">Your file id:</h2>
      <div id="idInput" className="inputs">
        <CopyToClipboard text={id}>
          <button className="copyButton" id="copy1">
            <p className="copyText">Copy</p>
          </button>
        </CopyToClipboard>
        <p id="textId">{id}</p>
      </div>
      <h2 id="encKey">Encrypt key:</h2>
      <div className="inputs" id="keyInput">
        <CopyToClipboard text={encode64(key)}>
          <button className="copyButton" id="copy2">
            <p className="copyText">Copy</p>
          </button>
        </CopyToClipboard>
        <p id="keyId">{encode64(key)}</p>
      </div>
    </div>
  );
};

export default FileData;
