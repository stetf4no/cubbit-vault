import React from "react";
import logo from "../assets/LogoAndBrand.svg";
import "./navBar.css";
import styled from "styled-components";
import EncryptedText from "./EncryptedText";
import store from "../redux/store";
import { setPlainModeAction } from "../redux/actions";
import { connect } from "react-redux";

const ButtonGroup = styled.div`
  position: absolute;
  width: 240px;
  height: 40px;
  left: calc(50% - 240px / 2 + 440px);
  top: 16px;

  background: #009eff;
  border-radius: 2px;
`;

const ToggleButton = styled.div`
  position: absolute;
  left: ${(props) => (props.active ? "0.83%" : "50.83%")};
  right: ${(props) => (props.active ? "50.83%" : "0.83%")};
  top: 5%;
  bottom: 5%;
  background: #363636;
  border-radius: 2px;
`;

const EncryptedButton = styled.button`
  position: absolute;
  left: 7%;
  right: 50%;
  top: 20%;
  bottom: 20%;

  font-family: Nunito;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 187.5%;
  background-color: transparent;
  border: 0px;
  display: flex;
  justify-content: center;
  text-align: center;
  color: #ffffff;
`;

const PlainButton = styled.button`
  position: absolute;
  left: 60%;
  right: 0%;
  top: 20%;
  bottom: 20%;

  font-family: Nunito;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 187.5%;
  background-color: transparent;
  border: 0px;
  display: flex;
  justify-content: center;
  text-align: center;
  color: #ffffff;
`;

const NavigationBar = (props) => {
  const { plainMode, encryptKey } = props;
  const encryptedText = "b-&+(2'";


  return (
    <nav className="navbar">
      <img id="logo" src={logo} alt="" />
      <ButtonGroup>
        <ToggleButton active={plainMode}></ToggleButton>
        <EncryptedButton onClick={() => store.dispatch(setPlainModeAction(false))}>
          Encrypted
        </EncryptedButton>
        <PlainButton onClick={() => store.dispatch(setPlainModeAction(true))}>
          <EncryptedText
            encryptKey={encryptKey}
            plainMode={plainMode}
            value={encryptedText}
          />
        </PlainButton>
      </ButtonGroup>{" "}
    </nav>
  );
};

const mapStateToProps = (state) => {
  const { plainMode, encryptKey } = state;
  return {
    plainMode,
    encryptKey,
  };
};

export default connect(mapStateToProps)(NavigationBar);
