import React, { useState } from "react";
import { connect } from "react-redux";
import NavigationBar from "./components/navBar";
import FileUploader from "./components/FileUploader";
import EncryptedText from "./components/EncryptedText";
import FileData from "./components/FileData";
import FileIdCkeck from "./components/FileIdCheck";
import FileDecrypt from "./components/FileDecrypt";

import "./App.css";
const dotenv = require("dotenv");
dotenv.config();

function App(props) {
  const title = "`4!!(3=s 4+3";
  const footText = `q'$=6'.+$=(2=-$5$1=3'$=24,=.%=3'$=/ 132=J=(3=(2=&1$ 3$1=.1=+$22$1I=#$/$-#(-&=.-='.6=6$++=3'$=(-#(5(#4 +2=6.1*=3.&$3'$1`;
  const { plainMode, encryptKey } = props;
  const [page, setPage] = useState(1);
  const [fileData, setFileData] = useState({});
  const [fileMetaData, setFileMetaData] = useState({});


  return (
    <div id="content">
      <NavigationBar />
      {page === 1 && (
        <FileUploader
          encryptKey={encryptKey}
          plainMode={plainMode}
          done={({ fileData, page }) => {
            setFileData(fileData);
            setPage(page);
          }}
        />
      )}
      {page === 2 && <FileData fileData={fileData} />}
      {page === 3 && (
        <FileIdCkeck
          done={({ fileMetaData, page }) => {
            setFileMetaData(fileMetaData);
            setPage(page);
          }}
        />
      )}
      {page === 31 && (
        <FileDecrypt fileMetaData={fileMetaData}
          done={({ fileData, page }) => {
            setPage(page);
          }}
        />
      )}
      <h2 id="h2">
        <EncryptedText
          encryptKey={encryptKey}
          plainMode={plainMode}
          value={title}
        />
      </h2>
      <h2 id="footText">
        <EncryptedText
          encryptKey={encryptKey}
          plainMode={plainMode}
          value={footText}
        />
      </h2>
    </div>
  );
}

const mapStateToProps = (state) => {
  const { plainMode, encryptKey } = state;
  return {
    plainMode,
    encryptKey,
  };
};

export default connect(mapStateToProps)(App);
