const port = 3112;
const express = require("express");
const path = require("path"); //used for file path
const fs = require("fs-extra"); //File System - for file manipulation
const cors = require("cors");
const fileUpload = require("express-fileupload");
const uuidv4 = require("uuid/v4");

const app = express();
const memoryStore = {};

app.use(
  fileUpload({
    createParentPath: true,
  })
);
app.use(cors());

const routerV1 = express.Router();

routerV1.post("/files", (req, res, next) => {
  try {
    if (!req.files) {
      res.send({
        status: false,
        message: "Error: No file uploaded",
      });
    } else {
      let uploadedFile = req.files.cubbitfile;
      const id = uuidv4();
      console.log("uploadedFile ", uploadedFile);
      uploadedFile.mv(`./files/${id}/${uploadedFile.name}`);
      memoryStore[id] = {
        name: uploadedFile.name,
        mimetype: uploadedFile.mimetype,
        size: uploadedFile.size,
      };
      res.json({
        message: `File ${uploadedFile.name} uploaded`,
        id,
      });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Error while uploading file." });
  }
});

routerV1.get("/files/:id/meta", (req, res, next) => {
  const id = req.params.id;
  const meta = memoryStore[id];
  if (meta) {
    res.json({ ...meta });
  } else {
    res.status(404);
  }
});

routerV1.get("/files/:id", (req, res, next) => {
  const id = req.params.id;
  console.log('files/:id',id)
  const meta = memoryStore[id];
  if (meta) {
    res.sendFile(`./files/${id}/${meta.name}`, { root: __dirname });
  } else {
    test.status(404);
  }
});

app.use("/v1", routerV1);

app.use("/", (req, res) => {
  console.log("/", req.body);
  res.send("Hello World!");
});

app.use(function (err, req, res, next) {
  console.log("Default error handler: ", err);
  res.status(500).send("Something went wrong!! " + JSON.stringify(err));
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
