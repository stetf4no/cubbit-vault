
Build the server application

### `yarn build-server`

Build client application
### `yarn install`

Start both client and server application
### `yarn start`

.env file configure server url 

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

